class Hotdog

	def intialize
		
	end
	
	def condiments
		"mayo"	
	end

end

require 'minitest/autorun'


class TestHotdog < Minitest::Test

	def setup
		@Hotdog = Hotdog.new
	end

	def test_what_is_on_my_hotdog
		assert_equal "mayo", @Hotdog.condiments
	end

end
